<!DOCTYPE html>
<html lang="en">
<?php  
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST']."/";     
   
    if($url == "http://localhost/"){
        $url.= "parul-university-consultancy-center/";
    }
    else if($url == "https://paruluniversity.ac.in/"){
        $url.= "consultancycenter/";
    }
    // echo "<script>alert('$url');</script>"; 

  ?>

<head>
    <title>Parul University | Consultancy Center</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/responsive.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <script src="jquery.min.js"></script>
</head>

<body class="consultancy-page" id="about-us" data-spy="scroll" data-target=".navbar" data-offset="120">

    <header id="header" class="fixed-top">


        <nav class="navbar navbar-light navbar-expand-lg w-100">
            <div class="container-fluid navbar-margin">

                <a class="navbar-brand float-md-left logo-wd" href="/"><img alt="logo" src="images/logo.svg"
                        class="img-fluid" />
                </a>
                <button class="navbar-toggler float-right mt-0" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#facilities">Our Facilities</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#services">Our Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#clients">Our Clients </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contactus">Contact Us</a>
                        </li>
                    </ul>
                </div>




            </div>
        </nav>
    </header>
    <!-- header ends -->

    <!-- main banner start -->

    <section class="banner-section" id="home">
        <div class="container-fluid px-0">
            <div class="row no-gutters">
                <div class="col-md-12">
                    <div class="position-relative">
                        <img src="images/Consultancy_Banner.jpg" alt="Parul University" class="w-100 img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- main banner end -->

    <!-- about section starts -->
    <section class="about bg-about-img py-5 mx-0" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="pb-1 text-white font-weight-bold">About Us</h1>
                    <img src="images/whitedot.svg" class="img-fluid">

                    <p class="text-white pt-3">Consultancy Center, Parul University is an initiative to provide quality
                        testing and consultancy services to various industries, academia and research organizations
                        across India. The center is involved in various tailor made research services as per the
                        requirement of industries. We offer a broad range of state-of-the-art analytical instruments
                        under one roof and facilities for accurate, prompt and cost-effective analysis and testing. We
                        have trained and qualified staff for sample preparation, testing and interpretation. The goal of
                        the centre is dedicated towards ensuring the highest form of credibility and quality in our
                        consultancy services. </p>
                </div>
            </div>
        </div>
    </section>
    <!-- about section ends -->

    <!-- consult section starts -->
    <section class="consult pt-5 pb-5 bg-consult-img" id="consult">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-violet font-weight-bold">Why Consult with us?</h1>

                    <img src="images/red-dot.svg" class="img-fluid">
                </div>
            </div>

            <div class="row justify-content-center align-items-end">
                <div class="col-md-3">
                    <div class="mt-5 text-center">
                        <img src="images/consult/world.png" class="img-fluid pb-3">
                        <p class="text-violet font-weight-600 slider-title">State of the Art Facilities</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mt-5 text-center">
                        <img src="images/consult/money.png" class="img-fluid pb-3">
                        <p class="text-violet font-weight-600 slider-title">Affordable and Reliable Services</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mt-5 text-center">
                        <img src="images/consult/leadership.png" class="img-fluid pb-3">
                        <p class="text-violet font-weight-600 slider-title">Leading Experts From Multiple Disciplines
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="mt-5 text-center">
                        <img src="images/consult/knowledge.png" class="img-fluid pb-3">
                        <p class="text-violet font-weight-600 slider-title">Exposure to Knowledge Resource</p>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="card w-100 h-100 animate-card">
                        <img class="card-img-top" src="images/consult/" alt="">
                        <div class="card-body">
                            <p class="card-text">State of the Art Facilities </p>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-md-3">
                    <div class="card w-100 h-100 animate-card">
                        <img class="card-img-top" src="images/consult/leadership.png" alt="">
                        <div class="card-body">
                            <p class="card-text">Leading Experts From Multiple Disciplines</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card w-100 h-100 animate-card">
                        <img class="card-img-top" src="images/consult/money.png" alt="">
                        <div class="card-body">
                            <p class="card-text">Affordable and Reliable Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card w-100 h-100 animate-card">
                        <img class="card-img-top" src="images/consult/knowledge.png" alt="">
                        <div class="card-body">
                            <p class="card-text">Exposure to Knowledge Resource</p>
                        </div>
                    </div>
                </div> -->
            </div>


        </div>
    </section>
    <!-- consult section ends -->

    <!-- leader section starts -->
    <section class="leader bg-leader-img pt-5 pb-lg-5">
        <div class="container">
            <div class="row mx-0">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-violet font-weight-bold">Our Objectives</h1>
                    <img src="images/red-dot.svg" class="img-fluid">
                </div>
            </div>
            <div class="row mt-2 mx-0">
                <div class="accordion">
                    <dl>
                        <dt><a class="accordionTitle" href="#">Research Support</a></dt>
                        <dd class="accordionItem accordionItemCollapsed">
                            <p>To support research in advanced areas of science and technology.</p>
                        </dd>
                        <dt><a href="#" class="accordionTitle">Additional Services </a></dt>
                        <dd class="accordionItem accordionItemCollapsed">
                            <p>To offer services to industries and other education and research organizations as
                                testing/consultancy services.</p>
                        </dd>
                        <dt><a href="#" class="accordionTitle">Innovative Development </a></dt>
                        <dd class="accordionItem accordionItemCollapsed">
                            <p>To offer support to R&D centres & industries towards design & development of their
                                products</p>
                        </dd>
                        <dt><a href="#" class="accordionTitle">Training & Capacity Building</a></dt>
                        <dd class="accordionItem accordionItemCollapsed">
                            <p>To arrange specialized training programs for technicians working on instruments in
                                academic institutions / small industries.</p>
                        </dd>
                        <dt><a href="#" class="accordionTitle">Developing Networks</a></dt>
                        <dd class="accordionItem accordionItemCollapsed">
                            <p>To build a strong industry academic network</p>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- leader section ends -->

    <!-- specilizations section starts -->
    <section class="facilities pt-2 bg-leader-img" id="facilities">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-violet font-weight-bold">Our State of the Art Facilities</h1>
                    <img src="images/red-dot.svg" class="img-fluid">
                </div>
            </div>

            <div class="row pt-5 justify-content-center">
                <div class="owl-carousel owl-theme pt-4" id="specilization_slide">
                    <div class="item">
                        <div class="mb-5 text-center">
                            <img src="images/facilities/Instrumentation Facilities.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Instrumentation <br>Facilities</p>
                        </div>
                        <div class="mt-5 text-center">
                            <img src="images/facilities/Microbiology Department.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Microbiology <br>Department</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="mb-5 text-center">
                            <img src="images/facilities/Environmental Audit Laboratory.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Environmental Audit  <br>Laboratory (Schedule I)</p>
                        </div>
                        <div class="mt-5 text-center">
                            <img src="images/facilities/Medical Diagnostic Facilities.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Medical Diagnostic Facilities<br> Public Testing Laboratory</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="mb-5 text-center">
                            <img src="images/facilities/Vehicle 3D Alignment Facilities.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Vehicle 3D Alignment <br>Facilities</p>
                        </div>
                        <div class="mt-5 text-center">
                            <img src="images/facilities/Center of Research for Development.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Center of Research <br>for Development
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        
                        <div class="mb-5 text-center">
                            <img src="images/facilities/Electrical Machines Laboratory.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Electrical Machines <br>Laboratory</p>
                        </div>

                        <div class="mt-5 text-center">
                            <img src="images/facilities/Animal Facilitation Centre.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Animal Facilitation <br>Centre</p>
                        </div>

                    </div>
                    <div class="item">
                        <div class="mb-5 text-center">
                            <img src="images/facilities/Advanced Manufacturing Process Laboratory.svg"
                                class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Advanced Manufacturing <br>Process
                                Laboratory</p>
                        </div>
                        <div class="mt-5 text-center">
                            <img src="images/facilities/Toxicological Research Centre.svg" class="img-fluid pb-3">
                            <p class="text-violet font-weight-600 slider-title">Toxicological Research <br>Centre</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="mb-5 text-center">
                                <img src="images/facilities/Industrial Automation &  Control Laboratory.svg"
                                    class="img-fluid pb-3">
                                <p class="text-violet font-weight-600 slider-title">Industrial Automation <br>& Control
                                    Laboratory</p>
                        </div>
                        <div class="mt-5 text-center">
                                <img src="images/facilities/Registered Ethical Committee for Animal - Human Research.svg"
                                    class="img-fluid pb-3">
                                <p class="text-violet font-weight-600 slider-title">Registered Ethical <br>Committee for
                                    Animal & Human
                                    Research </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="mb-5 text-center border-css-parent">
                            <div class="border-css">
                                <img src="images/facilities/microlab.png" class="img-fluid pb-3">
                            </div>
                            <p class="text-violet font-weight-600 slider-title">Micro Nano <br> Lab</p>
                        </div>
                        <div class="mt-5 text-center border-css-parent">
                            <div class="border-css">
                                <img src="images/facilities/fab-lab.png" class="img-fluid pb-3">
                            </div>
                            <p class="text-violet font-weight-600 slider-title">FAB Lab</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="mb-5 text-center border-css-parent">
                        <div class="border-css">
                            <img src="images/facilities/robo-lab.png"
                                class="img-fluid pb-3">
                        </div>
                            <p class="text-violet font-weight-600 slider-title">Robotics and Automation<br> Lab</p>
                        </div>
                        <div class="mt-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/arvr.png" class="img-fluid pb-3">
                            </div>
                            
                            <p class="text-violet font-weight-600 slider-title">AR/VR  Lab</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="mb-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/iotlab.png"
                                class="img-fluid pb-3">
                            </div>
                            
                            <p class="text-violet font-weight-600 slider-title">IoT (Internet of Things)<br> Lab</p>
                        </div>
                        <div class="mt-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/3d-scanner.png" class="img-fluid pb-3">
                            </div>
                            
                         
                            <p class="text-violet font-weight-600 slider-title">3D Printer & Scanner<br> Facility</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="mb-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/ayurveda.png"
                                class="img-fluid pb-3">
                            </div>
                            
                            <p class="text-violet font-weight-600 slider-title">Ayurveda – Product <br>development facility</p>
                        </div>
                        <div class="mt-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/patent.png" class="img-fluid pb-3">
                            </div>
                            
                            <p class="text-violet font-weight-600 slider-title">Patent Filing <br>services</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="mb-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/technology-transfer.png"
                                class="img-fluid pb-3">
                            </div>
                           
                            <p class="text-violet font-weight-600 slider-title">Technology Transfer <br>Services</p>
                        </div>
                        <div class="mt-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/formulation.png" class="img-fluid pb-3">
                            </div>
                            
                            <p class="text-violet font-weight-600 slider-title">Formulation development <br>Lab</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="mb-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/software.png"
                                class="img-fluid pb-3">
                            </div>
                           
                            <p class="text-violet font-weight-600 slider-title">Licensed Software for<br> respective areas </p>
                        </div>
                        <div class="mt-5 text-center border-css-parent">
                        <div class="border-css">
                        <img src="images/facilities/nanotechnology.png" class="img-fluid pb-3">
                            </div>
                            
                            <p class="text-violet font-weight-600 slider-title">Nano particle<br> Development</p>
                        </div>
                    </div>
                    
                </div>
            </div>


        </div>
    </section>
    <!-- specilizations section ends -->

    <!-- services section starts -->
    <section class="services" id="services">
        <div class="container">
            <div class="row mx-0">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-violet font-weight-bold">Our Range of Quality Services</h1>
                    <img src="images/red-dot.svg" class="img-fluid ">
                </div>
            </div>
            <div class="row mx-0 pt-3 services-pagination">
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/Medical and allied services.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Medicine</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Public Mental Health</li>
                                    <li>- Public Health Assessment  and Programme Evaluation</li>
                                    <li>- Health Behaviour Change Communication</li>
                                    <li>- Capacity building of health workers</li>
                                    <p class="read-more" data-toggle="modal" data-target="#service1">Read more..</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/Faculty-of-Architecture-and-Planning.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Architecture and Planning</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- Urban & Regional Planning</li>
                                    <li>- Rural Planning</li>
                                    <li>- Heritage Management</li>
                                    <li>- WASH (Water & Sanitation Hygiene)</li>
                                    <p class="read-more" data-toggle="modal" data-target="#service2">Read more..</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/faculty-of-pharmacy.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Pharmacy</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- Novel Drug Delivery systems</li>
                                    <li>- Preclinical Study</li>
                                    <li>- Solid oral dosage  development</li>
                                    <li>- Analytical Method development UV and HPLC</li>
                                    <p class="read-more" data-toggle="modal" data-target="#service3">Read more..</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/faculty-of-ayurveda.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Ayurveda</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- Therapeutic Yoga</li>
                                    <li>- Therapeutic Diet</li>
                                    <li>- Mental Health</li>
                                    <li>- Rejuvenation therapy</li>
                                    <p class="read-more" data-toggle="modal" data-target="#service4">Read more..</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/faculty-of-agriculture.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Agriculture</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- Agronomy</li>
                                    <li>- Horticulture</li>
                                    <li>- Genetics and Plant Breeding</li>
                                    <li>- Forestry and Environment Science</li>
                                    <p class="read-more" data-toggle="modal" data-target="#service5">Read more..</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/faculty-of-design.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Design & Fine Arts</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- Fashion Communication and Styling</li>
                                    <li>- Interior Design and Fine Arts</li>
                                    <li>- Visual Communication</li>
                                    <li>- Product Design</li>
                                    <p class="read-more" data-toggle="modal" data-target="#service6">Read more..</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/petrolium.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Engineering & Technology (PIT)</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled red-css">
                                    <li class="read-more" data-toggle="modal" data-target="#automobile-eng">- AUTOMOBILE/MECHANICAL</li>
                                    <li class="read-more" data-toggle="modal" data-target="#chemical-eng">- CHEMICAL</li>
                                    <li class="read-more" data-toggle="modal" data-target="#computer-sci-eng">- COMPUTER SCIENCE</li>
                                    <li class="read-more" data-toggle="modal" data-target="#agri-eng">- AGRICULTURAL</li>
                                    <li class="read-more" data-toggle="modal" data-target="#dairyfood-eng">- DAIRY & FOOD</li>
                                    <li class="read-more" data-toggle="modal" data-target="#petrolium-eng">- PETROLEUM</li>
                                    <li class="read-more" data-toggle="modal" data-target="#biotech-eng">- BIOTECHNOLOGY</li>
                                    <li class="read-more" data-toggle="modal" data-target="#biomed-eng">- BIOMEDICAL</li>
                                    <li class="read-more" data-toggle="modal" data-target="#mechtro-eng">- MECHATRONICS/ROBOTICS</li>
                                    <li class="read-more" data-toggle="modal" data-target="#pitcivil-eng">- CIVIL ENGINEERING</li>
                                    <li class="read-more" data-toggle="modal" data-target="#ash-eng">- ASH DEPARTMENT</li>
                                    
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/cs.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Engineering & Technology</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled red-css">
                                    <li class="read-more" data-toggle="modal" data-target="#civil-eng">- Civil Engineering</li>
                                    <li class="read-more" data-toggle="modal" data-target="#electrical-eng">- Electrical</li>
                                    <li class="read-more" data-toggle="modal" data-target="#it-eng">- Information Technology</li>
                                    <li class="read-more" data-toggle="modal" data-target="#aeronautical-eng">- Aeronautical</li>
                                    <li class="read-more" data-toggle="modal" data-target="#mechanical-eng">- Mechanical</li>
                                    <li class="read-more" data-toggle="modal" data-target="#cse-eng">- CSE</li>
                                    
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/Management.jpg" alt="Avatar" class="w-100">
                                <h4>Faculty of Management Studies</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled red-css">
                                    <li class="read-more" data-toggle="modal" data-target="#marketing">- Marketing</li>
                                    <li class="read-more" data-toggle="modal" data-target="#finance">- Finance</li>
                                    <li class="read-more" data-toggle="modal" data-target="#hr">- Human Resource</li>
                                    <li class="read-more" data-toggle="modal" data-target="#orm">- Operation Management</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
               
            </div>
            <div id="pagination-container"></div>

        </div>
    </section>
    <!-- services section ends -->
    <!--section faculty wise---->
    <section class="specilizations pt-5 pb-5 bg-leader-img" id="clients">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-violet font-weight-bold">Faculty Wise Consultancy Information</h1>

                    <img src="images/red-dot.svg" class="img-fluid">
                </div>
            </div>

            <div class="row justify-content-center" id="faculty_consultancy">
                <!--------->
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/Brochure - EE Department Consultancy Cell.pdf" target="_blank"><img src="images/services/eceng.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/Brochure of Micro-Nano Research & Development Centre.pdf" target="_blank"><img src="images/services/micro.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/Foot Pressure Analysis, Flyer.pdf" target="_blank"><img src="images/services/101.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/GaitOn Postural Analysis, Flyer.pdf" target="_blank"><img src="images/services/102.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/GaitOn Running Analysis, Flyer.pdf" target="_blank"><img src="images/services/103.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/GaitOn  Walking Analysis, Flyer.pdf" target="_blank"><img src="images/services/104.png"
                            alt="Avatar" class="w-100"></a>
                </div>

                <!--------->
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/hotal management.pdf" target="_blank"><img src="images/services/Group 1.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/HOTEL MANAGEMENT.pdf" target="_blank"><img src="images/services/Group 2.png"
                            alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox">
                    <a href="pdf/Commen Flyer for parul institute of nursing.pdf" target="_blank"><img
                            src="images/services/Group 3.png" alt="Avatar" class="w-100"></a>
                </div>

                <div class="col-md-4 cicardbox ">
                    <a href="pdf/faculty of agriculture.pdf" target="_blank"><img
                            src="images/consultancycenter/faculty_of_agriculture.png" alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/faculty of architecture & planing.pdf" target="_blank"><img
                            src="images/consultancycenter/faculty_of_architecture_planing.png" alt="Avatar"
                            class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox">
                    <a href="pdf/faculty of management studies.pdf" target="_blank"><img
                            src="images/consultancycenter/faculty_management_studies.png" alt="Avatar"
                            class="w-100"></a>
                </div>


                <div class="col-md-4 cicardbox ">
                    <a href="pdf/FACULTY OF PHYSIOTHERAPY.pdf" target="_blank"><img
                            src="images/consultancycenter/FACULTY_OF_PHYSIOTHERAPY.png" alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">
                    <a href="pdf/faculty of social work.pdf" target="_blank"><img
                            src="images/consultancycenter/faculty_of_social_work.png" alt="Avatar" class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox">
                    <a href="pdf/Facuty Of pharmacy.pdf" target="_blank"><img
                            src="images/consultancycenter/Facuty_Of_pharmacy.png" alt="Avatar" class="w-100"></a>
                </div>

                <div class="col-md-4 cicardbox ">
                    <a href="pdf/First Aid Management with CPR nursing.pdf" target="_blank"><img
                            src="images/consultancycenter/First_Aid_Management_with_CPR.png" alt="Avatar"
                            class="w-100"></a>
                </div>
                <div class="col-md-4 cicardbox ">

                </div>
                <div class="col-md-4 cicardbox ">

                </div>
            </div>
            <a href="#" id="ciloadMore">Load More</a>
        </div>
    </section>
    <!-- clients section starts -->
    <section class="specilizations pt-5 pb-5 bg-leader-img" id="clients">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-violet font-weight-bold">Our Satisfied Clients</h1>

                    <img src="images/red-dot.svg" class="img-fluid">
                </div>
            </div>

            <div class="row pt-5 justify-content-center">
                <div class="owl-carousel owl-theme pt-5 px-5" id="recruiter_slide">
                    <!-------------->
                   

                    <div class="item">
                        <img src="images/clients/bacancy_technology_logo.webp" class="img-fluid pb-5">
                        <img src="images/clients/DEEPAKNTR.NS-d5b31c00.webp" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/edge pharma.webp" class="img-fluid pb-5">
                        <img src="images/clients/FILSEP.webp" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/Kaizen switch gear logo.webp" class="img-fluid pb-5">
                        <img src="images/clients/logo_382_Yield Pro Earth Logo with Name.webp" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/mansi-logo-1-e1601374614761.webp" class="img-fluid pb-5">
                        <img src="images/clients/mccain foods.webp" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/omgene.webp" class="img-fluid pb-5">
                        <img src="images/clients/panchmahal steel.webp" class="img-fluid">
                    </div>
                    <!-------------->
                    <div class="item">
                        <img src="images/clients/Mylein-Health-Care.png" class="img-fluid pb-5">
                        <img src="images/clients/vasu-healthcare.png" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/ic-bio.png" class="img-fluid pb-5">
                        <img src="images/clients/curitex-medica.png" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/vital-care.png" class="img-fluid pb-5">
                        <img src="images/clients/sanjeevani-bio.png" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/apollo-hospital.png" class="img-fluid pb-5">
                        <img src="images/clients/bills-biotech.png" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/gufic-bio.png" class="img-fluid pb-5">
                        <img src="images/clients/concord-biotech.png" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/Extension-of-Analytical-Services .png" class="img-fluid pb-5">
                        <img src="images/clients/GPCB-Gujrat.png" class="img-fluid">
                    </div>

                    <div class="item">
                        <img src="images/clients/GOGujrat.png" class="img-fluid pb-5">
                        <img src="images/clients/avacon.png" class="img-fluid">

                    </div>
                    <div class="item">
                        <img src="images/clients/isro.png" class="img-fluid pb-5">
                        <img src="images/clients/setco.svg" class="img-fluid">
                    </div>
                    

                </div>
            </div>


        </div>
    </section>
    <section class="pb-5" id="">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="pb-3 text-violet font-weight-bold">Ongoing/Completed Consultancy Projects</h1>

                <img src="images/red-dot.svg" class="img-fluid">


            </div>
            <div class="container">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#engineering-projects">Engineering Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#pharmacy-projects">Pharmacy Projects </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#commerce-projects">Commerce Projects </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#design-projects">Design Projects </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#physiotherapy-projects">Physiotherapy Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#social-projects">Social Work Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#online-projects">Online Education Projects</a>
                        </li>
                        
                    </ul>

                    <div class="tab-content">
                        <div id="engineering-projects" class="tab-pane fade show active  table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <!-- <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Reimbursement of consultancy charges as per university norms for services rendered by the department of dairy food technology
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            Baroda Beverages Pvt Limited
                                        </td>
                                    </tr> -->
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Design & Development of Flexible Pipe Reversing Mechanism
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            Yield Pro Earth Private Limited
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td  ><p>
                                            2.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Phase-1 Software based Analysis for Fitness of Machineries
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            TCR
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td  ><p>
                                            3.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Tolerances and Dimensioning for staff member
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            M/S Shreno limited
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td  ><p>
                                            4.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            ENVIRONMENT AUDIT CONSULTANCY
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            PANCHMAHAL STEEL LIMITED
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td  ><p>
                                            5.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                        DEVELOPMENT OF TRAVEL SENSING DEVICE
                                        </ol>
                                        
                                        </td>
                                        <td>
                                        SETCO AUTO SYSTEMS
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td  ><p>
                                            6.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of Engineering </p>
                                            <p>Technology</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                        DEVELOPMENT OF CHARACTERIZATION OF PROCESSED SHAPE MEMORY ALLOY (SMA) COMPONENTS FOR SPACE APPLICATIONS
                                        </ol>
                                        
                                        </td>
                                        <td>
                                        INDIAN SPACE RESEARCH ORGANISATION (ISRO)
                                        </td>
                                    </tr>
                                </tbody>
                        
                            </table>
                        </div>
                        <div id="pharmacy-projects" class="tab-pane fade ">
                            <div class="table-responsive">
                                <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                    <thead>
                                        <th>Sr. No.</th>
                                        <th>Department</th>
                                        <th class="w-20">Consultancy details</th>
                                        <th class="w-20">Consultancy Contact details</th>
                                    </thead>
                                    <tbody>
                                        <tr valign="top">
                                            <td  ><p>
                                                1.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                Development of Surface functionalized Lactoferrin based Protein biotherapeutics for the treatment of Ovarian Cancer
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Gujarat State Biotechnology Mission, Gandhinagar
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <td  ><p>
                                                2.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                Development of block copolymeric micelles containing EGFR moieties for the treatment of Lung carcinoma
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                ALL INDIA Council of Technical Education
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <td  ><p>
                                                3.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                High speed homogenizer
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Omgene Life science Pvt. Ltd
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <td  ><p>
                                                3.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                Evaluation of efficacy and tolerability of Abhraloha tablets in iron deficiency anaemia – Phase IV study
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Shree Dhootapapeshwar Limited,135 Nanubhai Desai Road, Khetwadi, Mumbai - 400 004
                                            </td>
                                        </tr>
                                        
                                        <tr valign="top">
                                            <td  ><p>
                                                4.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                Formulation and evaluation of conventional tablets
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Edge Pharma Pvt ltd
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <td  ><p>
                                                5.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                Consultancy Project amount (2nd phase)
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Mansi Polymers Pvt Ltd
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <td  ><p>
                                                6.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                Convert para amino phenol paracetamol
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Deepak Nitrite Limited
                                            </td>
                                        </tr>
                                      
                                        <tr valign="top">
                                            <td  ><p>
                                                7.</p>
                                            </td>
                                            <td  ><p style="margin-bottom: 0in">
                                                Faculty of </p>
                                                <p>Pharmacy</p>
                                            </td>
                                            <td>
                                            <ol class="ordered-list">
                                                MICs and Zone of inhibition studies
                                            </ol>
                                            
                                            </td>
                                            <td>
                                                Dr vidhi Thakral Pg scholar Parul Institute of ayurved
                                            </td>
                                        </tr>
                                      
                                    </tbody>
                            
                                </table>
                            </div>
                           
                            <!-- <div class="d-flex justify-content-center align-items-center text-center">
                                <div class="col-md-3">
                                    <a href="pdf/Dr. Pinkal patel_2022-23.pdf" target="_blank">
                                    <img src="images/pdf.svg" class="img-fluid">
                                    <p class="text-center pt-2">Dr. Pinkal patel<br>2022-23</p>
                                    </a>
                                </div>
                                
                                <div class="col-md-3">
                                    <a href="pdf/Dr. Dipti Patel_2018-19.pdf" target="_blank">
                                        <img src="images/pdf.svg" class="img-fluid">
                                        <p class="text-center pt-2">Dr. Dipti Patel<br>2018-19</p>
                                    </a>
                                </div>
                                
                                <div class="col-md-3">
                                    <a href="pdf/Dr. Dipti patel_2017-18.pdf" target="_blank">
                                        <img src="images/pdf.svg" class="img-fluid">
                                        <p class="text-center pt-2">Dr. Dipti patel <br> 2017-18</p>
                                    </a>
                                </div>
                                
                                <div class="col-md-3">
                                    <a href="pdf/Dr. Snigdhadas Mandal_2016-17.pdf" target="_blank">
                                    <img src="images/pdf.svg" class="img-fluid">
                                    <p class="text-center pt-2">Dr. Snigdhadas Mandal<br>2016-17</p>
                                    </a>
                                </div>

                            </div> -->
                        </div>
                        <div id="commerce-projects" class="tab-pane fade table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Commerce</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Faculty on GST good & service tax accredited to ICAI management consultancy
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            Rohit Mangal Chartered Account Firm
                                        </td>
                                    </tr>

                                    <tr valign="top">
                                        <td  ><p>
                                            2.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Commerce</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Corporate training
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            BANCANCY TECHNOLOGY
                                        </td>
                                    </tr>

                                    <tr valign="top">
                                        <td  ><p>
                                            3.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Commerce</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Corporate Consultancy service with regards to company formation
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            CORPLIANCE CONSULTANCY LLP
                                        </td>
                                    </tr>
                                    
                                  
                                </tbody>
                        
                            </table>
                        </div>
                        <div id="design-projects" class="tab-pane fade show   table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Design</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Pre-mixing of Web Series Taj Mahal Season 2
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            Mavericks Post Pvt Ltd Mumbai
                                        </td>
                                    </tr>
                                </tbody>
                        
                            </table>
                        </div>
                        <div id="physiotherapy-projects" class="tab-pane fade show   table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Physiotherapy</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Ergonomic Program for Industrial Workers at Apollo Tyres Ltd, Limda Unit, Waghodia
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            Apollo Tyres Ltd, Limda Unit, Waghodia
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td  ><p>
                                            2.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Physiotherapy</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                        CONSULTANCY FOR ERGONOMIC PROGRAM ON PHYSIOTHERAPY FOOT, POSTURAL, RUNNING, WALKING
                                        </ol>
                                        
                                        </td>
                                        <td>
                                        KAIZEN SWITCHGEAR PRODUCTS
                                        </td>
                                    </tr>
                                </tbody>
                        
                            </table>
                        </div>
                        <div id="social-projects" class="tab-pane fade show   table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Faculty of </p>
                                            <p>Social Work</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            Life Satisfaction & Quality of Life of the Elderly Living in Old age homes : A Comparative Study of Western India and North East India
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            National Human Rights Commission, New Delhi
                                        </td>
                                    </tr>
                                </tbody>
                        
                            </table>
                        </div>
                        <div id="online-projects" class="tab-pane fade table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                            Centre of Distance and </p>
                                            <p>Online Education</p>
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                            CONSULTANCY ASSIGNMENT TO CENTRE FOR DISTANCE AND ONLINE EDUCATION AT PARUL UNIVERSITY ON ADVANCE MICROSOFT EXCEL
                                        </ol>
                                        
                                        </td>
                                        <td>
                                            MCCAIN FOODS INDIA LTD
                                        </td>
                                    </tr>
                                </tbody>
                        
                            </table>
                        </div>
                        <div id="food-dairy-projects" class="tab-pane fade table-responsive">
                            <table class="table mt-5 mb-3 table-bordered w-70 " cellpadding="7" cellspacing="0">
                                <thead>
                                    <th>Sr. No.</th>
                                    <th>Department</th>
                                    <th class="w-20">Consultancy details</th>
                                    <th class="w-20">Consultancy Contact details</th>
                                </thead>
                                <tbody>
                                    <tr valign="top">
                                        <td  ><p>
                                            1.</p>
                                        </td>
                                        <td  ><p style="margin-bottom: 0in">
                                             FACULTY OF FOOD & DAIRY
                                        </td>
                                        <td>
                                        <ol class="ordered-list">
                                        CONSULTANCY OF BEVERAGE DEVELOPMENT
                                        </ol>
                                        
                                        </td>
                                        <td>
                                        AVACON MULTICORP INDIA PVT LTD
                                        </td>
                                    </tr>
                                </tbody>
                        
                            </table>
                        </div>
                    </div>
                   
                </div>
        </div>
    </section>
    <!-- clients section ends -->

    <!-- footer starts -->
    <section class="copyright px-3" id="contactus">
        <div class=" pt-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="pb-3 text-white font-weight-bold">Contact us</h1>

                    <img src="images/whitedot.svg" class="img-fluid">
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 col-lg-6 pt-5">
                    <div class="col-lg-12">
                        <h3 class="text-red text-center font-weight-bold">Consultancy Center - Parul University</h3>
                    </div>
                    <div class=" mt-lg-0 mt-3 mt-md-4 pt-5 text-center Address">
                        <h4 class="font-weight-bold text-white">Address</h4>
                        <h6 class="text-white pt-3"> P.O.Limda, Ta.Waghodia<br>
                            Gujarat 391760</h6>
                    </div>
                    <div class="mt-lg-0 mt-3 mt-md-4 pt-5 text-center Get_in_Touch">
                        <h4 class="font-weight-bold text-white">Get in Touch With Us</h4>

                        <h6 class="pb-3 text-white pt-3"><img src="images/Call.svg" class="img-fluid pr-2" />
                            <!-- <a href="tel:+91-2668-260300" class="text-white">+91-2668-260300</a> -->
                            <a href="tel:+91-7486018533" class="text-white">+91-7486018533</a>
                        </h6>

                        <h6 class="pb-3 text-white pt-0"><img src="images/mail.svg" class="img-fluid pr-2" />
                            <!-- <a href="mailto:info@paruluniversity.ac.in"
                            class="text-white">info@paruluniversity.ac.in </a> -->
                            <a href="mailto:consultancy@paruluniversity.ac.in"
                                class="text-white">consultancy@paruluniversity.ac.in</a>
                        </h6>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 pt-5">
                    <div class="col-lg-12">
                        <h3 class="text-red text-center font-weight-bold">Have a query? Reach out to us!</h3>
                    </div>
                    <div class="card mt-5 mx-auto contact-us-footer bg-light">
                        <div class="card-body bg-light">
                            <div class="container">
                                <form id="contact-form" role="form">
                                    <div class="controls">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_name">Firstname*</label> -->
                                                    <input id="form_name" placeholder="Firstname*" type="text"
                                                        maxlength="20" name="name" class="v-name form-control"
                                                        required="required" data-error="Firstname is required.">
                                                    <div class="invalid-feedback">Please Enter a valid Firstname </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_lastname">Lastname*</label> -->
                                                    <input id="form_lastname" placeholder="Lastname*" maxlength="20"
                                                        type="text" name="surname" class="v-name form-control"
                                                        required="required" data-error="Lastname is required.">
                                                    <div class="invalid-feedback">Please Enter a valid Lastname </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_email">Email*</label> -->
                                                    <input id="form_email" type="email" maxlength="30"
                                                        placeholder="Email*" name="email" class="form-control"
                                                        required="required" data-error="Valid email is required.">
                                                    <div class="invalid-feedback">Please Enter a valid Email </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_phone">Phone*</label> -->
                                                    <input id="form_phone" type="number" maxlength="11"
                                                        placeholder="Phone*" name="phone" class="v-phone form-control"
                                                        required="required" data-error="Phone Number is required.">
                                                    <div class="invalid-feedback">Please Enter a valid Phone Number
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_need" class="Subject_label">Subject*</label> -->
                                                    <select id="form_Subject" name="subject" class="form-control"
                                                        required="required" data-error="Please specify your Subject.">
                                                        <option value selected disabled>Subject</option>
                                                        <option value="Job Opportunity">Job Opportunity</option>
                                                        <option value="Product Inquiry">Product Inquiry</option>
                                                        <option value="Influencer Collaboration">Influencer
                                                            Collaboration</option>
                                                        <option value="Vendor Collaboration">Vendor Collaboration
                                                        </option>
                                                        <option value="Business Collaboration">Business Collaboration
                                                        </option>
                                                        <option value="Other (Please specify in description)">Other
                                                            (Please specify in description)</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_need">How did you find out about us?*</label> -->
                                                    <select id="form_about_us" name="about_us" class="form-control"
                                                        required="required"
                                                        data-error="Please specify How did you find out about us?">
                                                        <option value="" selected disabled>How did you find out about
                                                            us?</option>
                                                        <option value="Performance Marketing Sr. Executive">Performance
                                                            Marketing Sr. Executive&nbsp;</option>
                                                        <option value="Business Analyst">Business Analyst&nbsp;</option>
                                                        <option value="Business Development Executive">Business
                                                            Development Executive&nbsp;</option>
                                                        <option value="Production Manager">Production Manager&nbsp;
                                                        </option>
                                                        <option value="Assocham">Assocham&nbsp;</option>
                                                        <option value="Ayush Events">Ayush Events&nbsp;</option>
                                                        <option value="CII MSME">CII MSME&nbsp;</option>
                                                        <option value="CII MSME Virtual">CII MSME Virtual&nbsp;</option>
                                                        <option value="CPHI">CPHI&nbsp;</option>
                                                        <option value="Drug Today Newspaper">Drug Today Newspaper&nbsp;
                                                        </option>
                                                        <option value="E-mail">E-mail&nbsp;</option>
                                                        <option value="Facebook">Facebook&nbsp;</option>
                                                        <option value="FiHi Exhibition">FiHi Exhibition&nbsp;</option>
                                                        <option value="Food and Beverage Ingredients Magazine">Food and
                                                            Beverage Ingredients Magazine&nbsp;</option>
                                                        <option value="Ingredients South Asia Magazine">Ingredients
                                                            South Asia Magazine&nbsp;</option>
                                                        <option value="Instagram">Instagram&nbsp;</option>
                                                        <option value="IPHEX Exhibition">IPHEX Exhibition&nbsp;</option>
                                                        <option value="Krunch Magazine">Krunch Magazine&nbsp;</option>
                                                        <option value="LinkedIn">LinkedIn&nbsp;</option>
                                                        <option value="Manufacturing Label behind a product">
                                                            Manufacturing Label behind a product&nbsp;</option>
                                                        <option value="Medical Times Newspaper">Medical Times
                                                            Newspaper&nbsp;</option>
                                                        <option value="Nutrify India">Nutrify India&nbsp;</option>
                                                        <option value="Nutrify Today">Nutrify Today&nbsp;</option>
                                                        <option value="Organic Internet Search">Organic Internet
                                                            Search&nbsp;</option>
                                                        <option value="Synnex Exhibition">Synnex Exhibition&nbsp;
                                                        </option>
                                                        <option value="Synnex Virtual">Synnex Virtual&nbsp;</option>
                                                        <option value="Twitter">Twitter&nbsp;</option>
                                                        <option value="Webinar">Webinar&nbsp;</option>
                                                        <option value="Word of Mouth">Word of Mouth&nbsp;</option>
                                                        <option value="Youtube">Youtube&nbsp;</option>
                                                        <option value="Zeon Associate">Zeon Associate&nbsp;</option>
                                                        <option value="Other (Please specify in description)">Other
                                                            (Please specify in description)&nbsp;</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="form_message">Message *</label> -->
                                                    <textarea id="form_message" name="message" maxlength="300"
                                                        class="form-control" placeholder="Write your message here."
                                                        rows="4" required="required"
                                                        data-error="Please, leave us a message."></textarea>
                                                    <div class="invalid-feedback">Please Enter a valid message</div>
                                                </div>

                                            </div>

                                            <div class="col-md-12 pb-4">
                                                <div class="g-recaptcha"
                                                    data-sitekey="6Lf7CegqAAAAAPqhKunfvlZHNvFT3PDNFlqqiizw"></div>
                                            </div>
                                            <div class="col-md-12">

                                                <button type="submit"
                                                    class="btn contact-us-submit btn-success btn-send  pt-2 btn-block"
                                                    value="Submit">
                                                    <div class="loader"></div> <span
                                                        class="submit_btn_text">Submit</span>
                                                </button>
                                            </div>

                                        </div>


                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row text-center mt-lg-5 mt-3">
                <div class="col-lg-12">
                    <h6 class="text-white">© Copyright 2022. All Rights Reserved by Parul University.</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- footer ends -->

    <!-- Modals starts for services -->
    

    <!-- Modals ends for services -->

    <!-- <button class="btn scroll" id="scroll1" style="display:none;"><i class="fa fa-angle-up"></i>
</button> -->

<?php include 'inc/popup.php'; ?>
    <!-----form validation script------>
    <script>
    $(document).ready(function() {
        function submit_validation() {
            if ($('.invalid-feedback').is(':visible')) {

                $('.contact-us-submit').addClass('disabled');
            } else {
                $('.contact-us-submit').removeClass('disabled');
            }
        }
        $("#contact-form .v-name").keyup(function() {
            $(this).parent().find('.invalid-feedback').hide();
            let regex = new RegExp("^[a-zA-Z_ ]*$");
            let s = this.value;
            let text = $(this).val();
            let length = text.length;

            if ((regex.test(s)) && (length < 20)) {} else {
                $(this).parent().find('.invalid-feedback').show();
            }
            submit_validation();
        });
        $("#contact-form #form_message").keyup(function() {
            $(this).parent().find('.invalid-feedback').hide();
            let regex = new RegExp("^[a-zA-Z,'-'' '\.0-9\n]+$");
            let s = this.value;
            let text = $(this).val();
            let length = text.length;
            if ((regex.test(s)) && (length < 300)) {} else {
                $(this).parent().find('.invalid-feedback').show();
            }
            submit_validation();
        });

        $("#contact-form .v-phone").keyup(function() {
            $(this).parent().find('.invalid-feedback').hide();
            let text = $(this).val();
            let length = text.length;
            if (length < 11) {} else {
                $(this).parent().find('.invalid-feedback').show();
            }
            submit_validation();
        });




        $("form#contact-form").on('submit', function(e) {
            e.preventDefault();
            $('.submit_btn_text').hide();
            $('.loader').show();
            $('.contact-us-submit').addClass('disabled');
            if (grecaptcha.getResponse() == "") {
                alert("Please Verify Recaptcha");
                $('.submit_btn_text').show();
                $('.loader').hide();
                $('.contact-us-submit').removeClass('disabled');
            } else {
                $.post("<?php echo $url;?>form-submit.php", $("#contact-form").serialize(), function(
                    data) {
                    //    alert("HERE");
                    console.log(data);
                    if (data == 'success') {
                        $('.card-body').html(
                            '<h4 class="thankyou-message">Thanks for contacting us! We will be in touch with you shortly.</h4>'
                        );

                    } else {
                        alert('something went wrong please try again');
                        // location.reload(true);
                    }
                });
            }
        });



    });
    </script>

    <style>
    .thankyou-message {
        color: #169a16;
    }

    ul.nav.nav-tabs {
        justify-content: center;
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        color: #393185;
        background-color: #fff;
        border-color: #c42c40 #dee2e6 #fff;
        border-top-width: 3px;
        font-weight: 500;
    }

    a.nav-link {
        font-weight: 500;
    }

    .ordered-list {
        padding-left: 1.5rem;
    }

    @media screen and (min-width: 900px) {

        .consultancy-page .consult,
        .consultancy-page .leader .container,
        .consultancy-page .leader {
            padding-bottom: 0px !important;
        }

        .consultancy-page .leader,
        .consultancy-page .facilities .container,
        .consultancy-page .services .container,
        .consultancy-page .specilizations .container {
            padding-top: 0px !important;
        }

        .consultancy-page .consult,
        .consultancy-page .facilities {
            padding-top: 80px !important;
        }
    }

    .contact-us-footer {
        max-width: 400px;
    }

    @media screen and (max-width: 980px) {
        .consultancy-page .navbar-light .navbar-toggler {
            margin-top: 0px !important;
            margin-right: 1rem !important;
        }

        .consultancy-page #collapsibleNavbar {
            margin-right: 1rem !important;
        }

        .consultancy-page .about p {
            padding: 10px !important;
        }

        body.consultancy-page .specilizations#clients,
        body.consultancy-page .leader {
            padding-top: 3rem !important
        }
    }

    @media screen and (max-width: 700px) {
        body section#services .container {
            padding-bottom: 0px !important;
        }
    }

    @media screen and (max-width: 500px) {
        section#services {
            padding-top: 3rem;
        }
    }

    .disabled {
        pointer-events: none;
        cursor: default;
        text-decoration: none;
    }

    .loader {
        border: 3px solid #f3f3f3;
        border-radius: 50%;
        border-top: 3px solid #00b83e00;
        width: 20px;
        height: 20px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        display: none;
        margin: auto;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    .table-bordered td,
    .table-bordered th {
        border: 1px solid #c42c40;
    }

    .table-bordered th {
        border: 1px solid #c42c40 !important;
        /* border-top: 2px solid #393185 !important; */
    }

    .table {
        border-radius: 10px;
    }

    @media all and (max-width: 900px) {
        .table-responsive {
            overflow-x: scroll;
            width: 95%;
        }

        .table {
            border-radius: 10px;
            width: 10%;
            margin: auto;
            margin-left: 2rem;
        }

        .table-bordered td,
        .table-bordered th {
            border: 1px solid #c42c40;
            /* font-size: 0.8rem; */
        }

        .table p {
            /* font-size: 0.8rem; */
        }

        .d-flex {
            flex-wrap: wrap;
            margin-top: 2rem;
        }
    }

    @media all and (max-width: 400px) {
        .table-responsive {
            overflow-x: scroll;
            width: 95%;

        }

        .table {
            border-radius: 10px;
            width: 8%;
            margin: auto;
            margin-left: 2rem;
        }

        .table-bordered td,
        .table-bordered th {
            border: 1px solid #c42c40;
            /* font-size: 0.7rem; */
        }

        .table p {
            /* font-size: 0.7rem; */
        }
    }
    </style>

    <script>
    (function(window) {
        'use strict';

        function classReg(className) {
            return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
        }
        var hasClass, addClass, removeClass;

        if ('classList' in document.documentElement) {
            hasClass = function(elem, c) {
                return elem.classList.contains(c);
            };
            addClass = function(elem, c) {
                elem.classList.add(c);
            };
            removeClass = function(elem, c) {
                elem.classList.remove(c);
            };
        } else {
            hasClass = function(elem, c) {
                return classReg(c).test(elem.className);
            };
            addClass = function(elem, c) {
                if (!hasClass(elem, c)) {
                    elem.className = elem.className + ' ' + c;
                }
            };
            removeClass = function(elem, c) {
                elem.className = elem.className.replace(classReg(c), ' ');
            };
        }

        function toggleClass(elem, c) {
            var fn = hasClass(elem, c) ? removeClass : addClass;
            fn(elem, c);
        }
        var classie = {
            hasClass: hasClass,
            addClass: addClass,
            removeClass: removeClass,
            toggleClass: toggleClass,
            has: hasClass,
            add: addClass,
            remove: removeClass,
            toggle: toggleClass
        };
        if (typeof define === 'function' && define.amd) {
            define(classie);
        } else {
            window.classie = classie;
        }
    })(window);
    var $ = function(selector) {
        return document.querySelector(selector);
    }
    var accordion = $('.accordion');
    accordion.addEventListener("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        if (e.target && e.target.nodeName == "A") {
            var classes = e.target.className.split(" ");
            if (classes) {
                for (var x = 0; x < classes.length; x++) {
                    if (classes[x] == "accordionTitle") {
                        var title = e.target;
                        var content = e.target.parentNode.nextElementSibling;
                        classie.toggle(title, 'accordionTitleActive');
                        if (classie.has(content, 'accordionItemCollapsed')) {
                            if (classie.has(content, 'animateOut')) {
                                classie.remove(content, 'animateOut');
                            }
                            classie.add(content, 'animateIn');
                        } else {
                            classie.remove(content, 'animateIn');
                            classie.add(content, 'animateOut');
                        }
                        classie.toggle(content, 'accordionItemCollapsed');
                    }
                }
            }
        }
    });
    </script>

    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/popper.min.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/wow.min.js"></script>
    <script defer src="owlCarousel/js/owl.carousel.min.js"></script>
    <script src="scripts/jquery.fancybox.min.js"></script>
    <script src="scripts/jquery.simplePagination.js"></script>
    <script type="text/javascript" src="scripts/scripts.js"></script>
    <script src="scripts/aos.js"></script>

    <script src="code.js"></script>
    <script src="form_submit.js"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>

</html>