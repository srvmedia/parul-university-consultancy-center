<?PHP
   // ini_set('display_errors', 1);
   // ini_set('display_startup_errors', 1);
   // error_reporting(E_ALL);

/*formanalytica starts*/
require_once 'requests/library/Requests.php';
Requests::register_autoloader();
/*formanalytica ends*/

     include("dbconfig.php");
          
     $current_date=date('d-m-y');
     $conn=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

     /*formanalytica starts*/
     function submit_lead_on_formanalytica($endpoint, $data) {
      $FORMANALYTICA_ENDPOINT = 'https://formanalytica.com/api/v1/user/business_user/'.$endpoint;
      $token = 'f7bc722fc08dcc7f9883c94fcb6a9e49';
      $headers = array('dmtpv-id' => 'pumba_reg2021');
      $response = Requests::post($FORMANALYTICA_ENDPOINT, $headers, $data);
      
      if ($response) {
          
          if ($response->status_code == 200) {
              return true;
          }
      }
      return false;
  }
  /*formanalytica ends*/


  if (mysqli_connect_errno())
        {
          echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        else{
   
       $name=htmlspecialchars($_POST['name']);
       $email=htmlspecialchars($_POST['email']);
       $mobile=htmlspecialchars($_POST['mobile']);
       $state=htmlspecialchars($_POST['inputState']); 
       $district=htmlspecialchars($_POST['inputDistrict']);
       $specialization=htmlspecialchars($_POST['specialization']);  
       $utm_source=htmlspecialchars($_POST['utm_source']);
       $utm_medium=htmlspecialchars($_POST['utm_medium']);
       $utm_campaign=htmlspecialchars($_POST['utm_campaign']);

       $text1="Hello $name They Say Time Is Money, But For Us Time Is Education, Choose to Invest in Your Education Our seats are almost filled up! Apply and secure your future with Parul University today! Continue your application process today and secure your place at Western India's Best Private University. Login to admissions.paruluniversity.ac.in and complete your application today Choose PU to Experience: Endless Global Opportunities | Enriching Cultural Exposure | Cutting-Edge Research Choose More Choose Right";

       $text2="Dear $name Thank you for showing your interest in Parul University. You are just a step away from enrolling at the Best Private University in Western India. Come and be a part of the success story. our counselors will contact you soon. You have registered for MBA and Login in on www.puadmission.in to complete your registration process For more details visit www.paruluniversity.ac.in For free career counselling call 18001231104";
       
     $sql="INSERT INTO `enquiry` (`name`,`email`,`mobile`,`state`,`district`,`specialization`,`utm_source`,`utm_medium`,`utm_campaign`,`create_date`) VALUES ('$name','$email','$mobile','$state','$district','$specialization','$utm_source','$utm_medium','$utm_campaign','$current_date')";


     echo $sql;
     
        if (mysqli_query($conn,$sql))
        {

          /*formanalytica starts*/
            $formanalytica_data = array(
                              'first_name' => $name,
                              'email' => $email,
                              'phone' => $mobile,
                              'state' => $state,
                              'city' => $district,
                              'specialization' => $specialization,
                              'utm_source' => $utm_source,
                              'utm_medium' => $utm_medium,
                              'utm_campaign' => $utm_campaign,
                              'is_verified' => false,
                            );
                            // submit_lead_on_formanalytica('submit_lead/', $formanalytica_data);
           /*formanalytica ends*/
         
           /*CRM starts*/

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://admissions.paruluniversity.ac.in/api/leadcapture",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{\n  \"country_code\": \"+91\",\n  \"phone\": \"$mobile\",\n  \"source\": \"Google SRV\",\n  \"name\": \"$name\",\n  \"country\": \"India\",\n  \"state\": \"$state\",\n  \"city\": \"$district\",\n  \"course\": \"$specialization\",\n  \"program\": \"1\",\n  \"email\": \"$email\",\n  \"father_name\": \"\",\n  \"dob\": \"\",\n  \"gender\": \"\",\n  \"address\": \"\",\n  \"nationality\": \"Indian\",\n  \"father_occupation\": \"\",\n  \"utm_source\": \"$utm_source\",\n  \"utm_content\": \"\",\n  \"utm_term\": \"\",\n  \"utm_campaign\": \"$utm_campaign\",\n  \"utm_medium\": \"$utm_medium\"\n}",
              CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 8e2dd7fc-abf4-18da-aa40-3c9999cd325b"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              //echo $response;
              //$res=array("status"=>"1"); 
              //echo json_encode($res);

              /*SMS START*/
              $curl = curl_init();

              curl_setopt_array($curl, array(
                CURLOPT_URL => "https://onlysms.co.in/api/jsonapi.aspx",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\n   \"FORMAT\":\"2\",\n    \"USERNAME\": \"parulinsbrc\",\n    \"PASSWORD\": \"pudemr@2017\",\n    \"GSMID\": \"eParul\",\n    \"TEXTTYPE\": \"TEXT\",\n    \"SMSDATA\" : [\n        {\"TEXT\" : \"$text1\", \"MOBLIST\": \"$mobile\" },\n        {\"TEXT\" : \"$text2\", \"MOBLIST\": \"$mobile\" }\n\t]\n}",
                CURLOPT_HTTPHEADER => array(
                  "cache-control: no-cache",
                  "content-type: application/json",
                  "postman-token: 00fc791c-a925-4b33-67c4-3153ff7661c7"
                ),
              ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              curl_close($curl);

              if ($err) {
                echo "cURL Error #:" . $err;
              } else {
                //echo $response;
                $res=array("status"=>"1"); 
                echo json_encode($res);
              }
              /*SMS END*/
            }
           /*CRM ends*/

   
        }
        else
        {
   
          $res=array("status"=>$sql);
          echo json_encode($res);
        }
     }
   ?>

