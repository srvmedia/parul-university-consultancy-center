<div class="modal fade service-modals" id="service1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Faculty of Medicine</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Public Mental Health</li>
                    <li>Public Health Assessment  and Programme Evaluation</li>
                    <li>Health Behaviour Change Communication</li>
                    <li>Capacity building of health workers</li>
                    <li>Public Health Interventions</li>
                    <li> Research and data analysis</li>
                    <li> Health Policy and System Research</li>
                    <li> Quality standards in healthcare</li>
                    <li> Antimicrobial Resistance</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade service-modals" id="service2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Faculty of Architecture and Planning</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Urban & Regional Planning</li>
                    <li>Rural Planning</li>
                    <li>Heritage Management</li>
                    <li>WASH (Water & Sanitation Hygiene)</li>
                    <li>Waste Management</li>
                    <li>Large Scale National-level Infrastructure Facilities</li>
                    <li>Environmental Planning</li>
                    <li>Transportation Planning</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade service-modals" id="service3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Faculty of Pharmacy</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Novel Drug Delivery systems</li>
                    <li>Preclinical Study</li>
                    <li>Solid oral dosage  development</li>
                    <li>Analytical Method development UV and HPLC</li>
                    <li>Pharmaceutical  Statistics and Design of Experiments</li>
                    <li>Structure Elucidation via  FTIR</li>
                    <li>Particle Size analysis via Zeta  Size</li>
                    <li>Isolation and Extraction of Phyto constituents and  Neutra-ceuticals</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade service-modals" id="service4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Faculty of Agriculture</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Therapeutic Yoga</li>
                    <li>Therapeutic Diet</li>
                    <li>Mental Health</li>
                    <li>Rejuvenation therapy</li>
                    <li>Immunity boosting</li>
                    <li>Obstetrics and Gynecology</li>
                    <li>Pediatric care</li>
                    <li>Ophthalmic care</li>
                    <li>Panchakarma</li>
                    <li>New dosage forms</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade service-modals" id="service5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Faculty of Agriculture</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Agronomy</li>
                    <li>Horticulture</li>
                    <li>Genetics and Plant Breeding</li>
                    <li>Forestry and Environment Science</li>
                    <li>Soil Science</li>
                    <li>Plant Protection</li>
                    <li>Social Science</li>

                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade service-modals" id="service6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Faculty of Design & Fine Arts</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Fashion Communication and Styling</li>
                    <li>Interior Design and Fine Arts</li>
                    <li>Visual Communication</li>
                    <li>Product Design</li>
                    <li>Animation and Film Making</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="automobile-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">AUTOMOBILE/MECHANICAL ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Wheel balancing & Alignment</li>
                    <li>Robotics</li>
                    <li>Rapid Prototyping</li>
                    <li>CREO</li>
                    <li>MATLAB</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="chemical-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">CHEMICAL ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                <li>Wastewater Testing, Treatment & Design - Conventional & Advanced Processe</li>
                <li>Catalysis</li>
                <li>Nanotechnology </li>
                <li>Microbial Fuel Cells and Wastewater Treatment</li>
                <li>MATLAB Programming and Simulation</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="computer-sci-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">COMPUTER SCIENCE & ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>IOT</li>
                    <li>Cyber Security</li>
                    <li>Visual Communication</li>
                    <li>Software Engineering</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="agri-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">AGRICULTURAL ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Micro irrigation system</li>
                    <li>Suggest methods to reduce drudgery in farming operation</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="dairyfood-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">DAIRY & FOOD TECHNOLOGY</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Quality Assurance</li>
                    <li>Research and development in food and dairy</li>
                    <li>Food packaging</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="petrolium-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">PETROLEUM ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Crude Oil Characterization</li>
                    <li>Reservoir Engineering</li>
                    <li>Drilling Fluid </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="biotech-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">BIOTECHNOLOGY ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Spectroscopy Analysis</li>
                    <li>PCR Analysis</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="biomed-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">BIOMEDICAL ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Medical Instrumentation</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="mechtro-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">MECHATRONICS/ROBOTICS ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>IOT</li>
                    <li>CAD/CAM-based solution</li>
                    <li>3D printing</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="pitcivil-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">CIVIL ENGINEERING</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Water and Wastewater analysis</li>
                    <li>Material testing - concrete, brick, cement</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="ash-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">ASH DEPARTMENT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Material Science</li>
                    <li>Nanomaterial synthesis</li>
                    <li>Analytical services</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="civil-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Civil Engineering</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="">
                    <li>Material Testing</li>
                    <li>Structure design</li>
                    <li>Construction Project</li>
                    <li>Water resource</li>
                    <li>Transportation</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="electrical-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Electrical Engineering</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Consultancy & Project</li>
                    <li>Embedded System and Internet of Things</li>
                    <li>Surveillance System</li>
                    <li>Communication System</li>
                    <li>Chip Design /VLSI</li>
                   
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="it-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Information Technology</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Data Analysis</li>
                    <li>Software development</li>
                    <li>Security, Trust and Privacy</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="aeronautical-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Aeronautical Engineering</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Drone Technology</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="mechanical-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Mechanical Engineering</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Renewable Energy Solutions</li>
                    <li>Design and Product development</li>
                    <li>HVAC System Design in Buildings</li>
                    <li>Development of Environmental Friendly Technologies</li>
                    <li>Supplier’s Design Verification</li>
                    <li>Material Resource Planning</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="cse-eng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Computer Science and Engineering</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Software Development</li>
                    <li>Networks</li>
                    <li>Security, Privacy</li>
                    <li>Smart City</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="marketing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Marketing</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Advances in Marketing and its applications in Business</li>
                    <li>Digital Marketing Strategies using Social Media</li>
                    <li>E- Retailing – A growth platform</li>
                    <li>Branding and Advertising for institutions and industry</li>
                    <li>Services Marketing Approach for institutions, corporations and industry</li>
                    <li>Consumer Behaviour</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="finance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Finance</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Microfinance – Small Scale Finance in Rural Sector</li>
                    <li>Financial Management in Institutions and Industry</li>
                    <li>Working Capital management</li>
                    <li>Database Management – Data for decision making</li>
                    <li>Tools in Data Analytics – Use of advanced applications like Excel, Python, R</li>
                    <li>Risk Management and Investment</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="hr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Human Resource</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Human Resource Practices in Institutions and Industry</li>
                    <li>Strategic Human Resource Management in Institutions and Industry</li>
                    <li>Contemporary HR Training in varied Institutions and Industry</li>
                    <li>Strategic Applications of Digital Technologies in HRM</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade service-modals" id="orm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Operation Management</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class=" ">
                    <li>Project Management in areas of Oil & Gas, Power, Fertilizer sector</li>
                    <li>Operations Management in areas of Oil & Gas, Power, Fertilizer sector</li>
                    <li>Supply Chain Management in areas of Oil & Gas, Power, Fertilizer sector</li>
                    <li>Decision making models using optimization Techniques</li>
                    <li>Application of Optimization techniques in Institutes, social organisations and Industry</li>
                    <li>Database management and data warehousing for Institutional data based decisions</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>