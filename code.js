$("#submit_btn").click(function(e) {

    e.preventDefault();
    var name = $.trim($("#name").val());
    var email = $.trim($("#email").val());
    var mobile_no = $.trim($("#mobile").val());
    var state = $.trim($("#inputState").val());
    var district = $.trim($("#inputDistrict").val());
    var specialization= $.trim($("#specialization").val());
    var course= $.trim($("#course").val());
   
    var utm_source = $.trim($("#utm_source").val());
    var utm_medium = $.trim($("#utm_medium").val());
    var utm_campaign = $.trim($("#utm_campaign").val());

 
     if (!name) {
        $('#allerror').html("Enter Your Full Name");
        return false;
     }
     if (/[^a-zA-Z \-]/.test(name)) {
        $('#allerror').html("Enter only alphabets in name");
        return false;
     }


      if (!email) {
          $('#allerror').html("Enter your Email ID");
          return false;
      }
      var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
      if (emailReg.test(email) == false)
      {
        $('#allerror').html("Enter Valid Email ID");
        return false;
       }  


      var mob = /^[0-9]{10}$/;
      if(!mobile_no)
      {
          $('#allerror').html("Enter Your Phone Number")
          return false;
      }
      if (mob.test(mobile_no) == false)
      {
          $('#allerror').html("Enter only 10 digits in Phone Number")
          return false;
      }
             
      if (!state)
      {
          $('#allerror').html("Select State");
          return false;
      }
      
      if (!district)
      {
          $('#allerror').html("Select City");
          return false;
      }

      if (!specialization)
      {
          $('#allerror').html("Select Specialization");
          return false;
      }

      if (grecaptcha.getResponse() == "") {
        e.preventDefault();
        $('#allerror').html("Please verify your captcha");
        return false;
    }

    $("#submit_btn").prop('disabled', true);
    $.ajax({

        url: "submitenquiry.php",
        type: 'POST',
        dataType: 'json',
        data: {name:name,email:email,mobile:mobile_no,inputState:state,inputDistrict:district,course:course,specialization:specialization,utm_source:utm_source,utm_medium:utm_medium,utm_campaign:utm_campaign}, 
        type: 'POST',
        success: function(response)
        {
             console.log(response)
             if(response.status=="1")
              {
                     window.location.href = "thankyou.php?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign ;
                } else 
                {
                    alert("please try again."); 
                }
        }
 });    
});


/*Mobile view*/

$("#submit_btn1").click(function(e) {

    e.preventDefault();
    var name = $.trim($("#name1").val());
    var email = $.trim($("#email1").val());
    var mobile_no = $.trim($("#mobile1").val());
    var state = $.trim($("#inputState1").val());
    var district = $.trim($("#inputDistrict1").val());
    var specialization= $.trim($("#specialization1").val());
    var course= $.trim($("#course1").val());
   
    var utm_source = $.trim($("#utm_source").val());
    var utm_medium = $.trim($("#utm_medium").val());
    var utm_campaign = $.trim($("#utm_campaign").val());

 
     if (!name) {
        $('#allerror1').html("Enter Your Full Name");
        return false;
     }
     if (/[^a-zA-Z \-]/.test(name)) {
        $('#allerror1').html("Enter only alphabets in name");
        return false;
     }


      if (!email) {
          $('#allerror1').html("Enter your Email ID");
          return false;
      }
      var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
      if (emailReg.test(email) == false)
      {
        $('#allerror1').html("Enter Valid Email ID");
        return false;
       }  


      var mob = /^[0-9]{10}$/;
      if(!mobile_no)
      {
          $('#allerror1').html("Enter Your Phone Number")
          return false;
      }
      if (mob.test(mobile_no) == false)
      {
          $('#allerror1').html("Enter only 10 digits in Phone Number")
          return false;
      }
             
      if (!state)
      {
          $('#allerror1').html("Select State");
          return false;
      }
      
      if (!district)
      {
          $('#allerror1').html("Select City");
          return false;
      }

      if (!specialization)
      {
          $('#allerror1').html("Select Specialization");
          return false;
      }
      
      if (grecaptcha.getResponse() == "") {
        e.preventDefault();
        $('#allerror1').html("Please verify your captcha");
        return false;
    }

    $("#submit_btn1").prop('disabled', true);
    $.ajax({

        url: "submitenquiry.php",
        type: 'POST',
        dataType: 'json',
        data: {name:name,email:email,mobile:mobile_no,inputState:state,inputDistrict:district,course:course,specialization:specialization,utm_source:utm_source,utm_medium:utm_medium,utm_campaign:utm_campaign}, 
        type: 'POST',
        success: function(response)
        {
             console.log(response)
             if(response.status=="1")
              {
                     window.location.href = "thankyou.php?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign ;
                } else 
                {
                    alert("please try again."); 
                }
        }
 });    
});

