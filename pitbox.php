<div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/automobile.jpg" alt="Avatar" class="w-100">
                                <h4>AUTOMOBILE/MECHANICAL ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- Wheel balancing & Alignment</li>
                                    <li>- Robotics</li>
                                    <li>- Rapid Prototyping</li>
                                    <li>- CREO</li>
                                    <li>- MATLAB</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/ce.jpg" alt="Avatar" class="w-100">
                                <h4>CHEMICAL ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Wastewater Testing, Treatment & Design - Conventional & Advanced Processe</li>
                                    <li>- Catalysis</li>
                                    <li>- Nanotechnology </li>
                                    <li>- Microbial Fuel Cells and Wastewater Treatment</li>
                                    <li>- MATLAB Programming and Simulation</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/cs.jpg" alt="Avatar" class="w-100">
                                <h4>COMPUTER SCIENCE & ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                
                                    <li>- IOT</li>
                                    <li>- Cyber Security</li>
                                    <li>- Visual Communication</li>
                                    <li>- Software Engineering</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/agriculture.jpg" alt="Avatar" class="w-100">
                                <h4>AGRICULTURAL ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Micro irrigation system</li>
                                    <li>- Suggest methods to reduce drudgery in farming operation</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/foodtech.jpg" alt="Avatar" class="w-100">
                                <h4>DAIRY & FOOD TECHNOLOGY</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Quality Assurance</li>
                                    <li>- Research and development in food and dairy</li>
                                    <li>- Food packaging</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/petrolium.jpg" alt="Avatar" class="w-100">
                                <h4>PETROLEUM ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Crude Oil Characterization</li>
                                    <li>- Reservoir Engineering</li>
                                    <li>- Drilling Fluid </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/biotech.jpg" alt="Avatar" class="w-100">
                                <h4>BIOTECHNOLOGY ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Spectroscopy Analysis</li>
                                    <li>- PCR Analysis</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/biomed.jpg" alt="Avatar" class="w-100">
                                <h4>BIOMEDICAL ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Medical Instrumentation</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/roboticseng.jpg" alt="Avatar" class="w-100">
                                <h4>MECHATRONICS/ROBOTICS ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- IOT</li>
                                    <li>- CAD/CAM-based solution </li>
                                    <li>- 3D printing </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/civileng.jpg" alt="Avatar" class="w-100">
                                <h4>CIVIL ENGINEERING</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Water and Wastewater analysis</li>
                                    <li>- Material testing - concrete, brick, cement</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <img src="images/services/ashdept.jpg" alt="Avatar" class="w-100">
                                <h4>ASH DEPARTMENT</h4>
                            </div>
                            <div class="flip-card-back">
                                <ul class="list-unstyled">
                                    <li>- Material Science</li>
                                    <li>- Nanomaterial synthesis </li>
                                    <li>- Analytical services</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>